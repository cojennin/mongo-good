#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>
#include "mongoc.h"

#define DEFAULT_MONGO_ADDR "127.0.0.1"
#define STR_REPLACE_BUFFER_SIZE 8192

typedef struct {
  ngx_str_t     mongo_server_addr;
  ngx_str_t     mongo_port;
  ngx_str_t     mongo_database;
  ngx_str_t     mongo_collection;
  ngx_str_t     mongo_user;
  ngx_str_t     mongo_passw;
} mongo_server;

typedef struct {
  mongo_server            mongo;
  mongoc_client_t         *client;
  mongoc_collection_t     *collection;
  mongoc_uri_t            *uri;
  bson_t                  *unreadable_fields;
  ngx_flag_t              allow_posting;
  ngx_flag_t              allow_aggregation;
  mongoc_read_prefs_t     *read_pref;
} ngx_http_mg_conf_t;

typedef struct {
  ngx_str_t   q;
  ngx_str_t   aggregation;
  ngx_str_t   limit;
  ngx_str_t   offset;
} query_params;

typedef struct {
  ngx_http_complex_value_t    error_msg;
  ngx_int_t                   error_code;
} error_s;

//Conf functions.
static char* ngx_http_mg_unreadable_fields(ngx_conf_t*, ngx_command_t*, void*);
static char* ngx_http_mg_read_preferences(ngx_conf_t*, ngx_command_t*, void*);
static char* ngx_http_mg(ngx_conf_t *, ngx_command_t *, void *);

static ngx_int_t ngx_http_mg_handler(ngx_http_request_t *);

static void* ngx_http_mg_create_loc_conf(ngx_conf_t *);
static char* ngx_http_mg_merge_loc_conf(ngx_conf_t *, void *, void *);

static ngx_int_t ngx_http_mg_handle_get_request(ngx_http_request_t*, ngx_http_complex_value_t*, error_s*);
static void ngx_http_mg_handle_post_request(ngx_http_request_t*);

static mongoc_uri_t* ngx_http_create_mongo_conn_uri(mongo_server);
static void ngx_http_mg_error(error_s*, char*, ngx_int_t, ngx_http_request_t*);
static void ngx_http_mg_error_from_bson_error(error_s*, bson_error_t*, ngx_http_request_t*);
static u_char* bson_to_json (ngx_http_request_t*, mongoc_cursor_t *, size_t *length, size_t *count, bson_error_t*);
static void recurse_bson_to_json (ngx_http_request_t*, const bson_t *bson, bson_string_t* str, int is_array);
//static int replace_str(char*, char*, char*);

static ngx_str_t application_type[] = {
  ngx_string("application/json"),
  ngx_string("application/javascript")
};

/*static ngx_str_t to_find[] = {
  ngx_string("\""),
  ngx_string("__%22__"),
  ngx_string("\n")
};

static ngx_str_t to_replace[] = {
  ngx_string("__%22__"),
  ngx_string("\\\""),
  ngx_string("\\n")
};*/

static ngx_command_t ngx_http_mg_commands[] = {
  { ngx_string("mongo"),
    NGX_HTTP_LOC_CONF|NGX_CONF_NOARGS|NGX_CONF_ANY,
    ngx_http_mg,
    NGX_HTTP_LOC_CONF_OFFSET,
    0,
    NULL
  },
  { ngx_string("mongo_user"),
    NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1,
    ngx_conf_set_str_slot,
    NGX_HTTP_LOC_CONF_OFFSET,
    offsetof(ngx_http_mg_conf_t, mongo.mongo_user),
    NULL
  },
  { ngx_string("mongo_pass"),
    NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1,
    ngx_conf_set_str_slot,
    NGX_HTTP_LOC_CONF_OFFSET,
    offsetof(ngx_http_mg_conf_t, mongo.mongo_passw),
    NULL
  },
  { ngx_string("mongo_db"),
    NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1,
    ngx_conf_set_str_slot,
    NGX_HTTP_LOC_CONF_OFFSET,
    offsetof(ngx_http_mg_conf_t, mongo.mongo_database),
    NULL
  },
  { ngx_string("mongo_collection"),
    NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1,
    ngx_conf_set_str_slot,
    NGX_HTTP_LOC_CONF_OFFSET,
    offsetof(ngx_http_mg_conf_t, mongo.mongo_collection),
    NULL
  },
  { ngx_string("mongo_unreadable_field"),
    NGX_HTTP_LOC_CONF|NGX_CONF_ANY,
    ngx_http_mg_unreadable_fields,
    NGX_HTTP_LOC_CONF_OFFSET,
    0,
    NULL
  },
  { ngx_string("mongo_allow_posting"),
    NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1,
    ngx_conf_set_flag_slot,
    NGX_HTTP_LOC_CONF_OFFSET,
    offsetof(ngx_http_mg_conf_t, allow_posting),
    NULL
  },
  { ngx_string("mongo_allow_aggregation"),
    NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1,
    ngx_conf_set_flag_slot,
    NGX_HTTP_LOC_CONF_OFFSET,
    offsetof(ngx_http_mg_conf_t, allow_aggregation),
    NULL
  },
  { ngx_string("mongo_read_preference"),
    NGX_HTTP_LOC_CONF|NGX_CONF_ANY,
    ngx_http_mg_read_preferences,
    NGX_HTTP_LOC_CONF_OFFSET,
    0,
    NULL
  },
  ngx_null_command
};

static ngx_http_module_t ngx_http_mg_module_ctx = {
  NULL,
  NULL, //postconfiguration

  NULL,
  NULL,

  NULL,
  NULL,

  ngx_http_mg_create_loc_conf,
  ngx_http_mg_merge_loc_conf
};

ngx_module_t ngx_http_mg_module = {
  NGX_MODULE_V1,
  &ngx_http_mg_module_ctx,
  ngx_http_mg_commands,
  NGX_HTTP_MODULE,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NGX_MODULE_V1_PADDING
};

static ngx_int_t
ngx_http_mg_handler(ngx_http_request_t *r)
{
    ngx_int_t    rc;
    ngx_http_complex_value_t cv;
    error_s mg_error;
    ngx_http_mg_conf_t *mgcf = ngx_http_get_module_loc_conf(r, ngx_http_mg_module);

    //Handling GET and POST
    if (!(r->method & (NGX_HTTP_GET|NGX_HTTP_POST))) {
        return NGX_HTTP_NOT_ALLOWED;
    }

    if (r->headers_in.if_modified_since) {
        return NGX_HTTP_NOT_MODIFIED;
    }

    //If we've got a GET request, process it
    if (r->method == NGX_HTTP_GET) {
      if (ngx_http_mg_handle_get_request(r, &cv, &mg_error) == NGX_ERROR) {
        //Error will be populated by ngx_http_mg_handle_get
        goto failed;
      }
    } else if (r->method == NGX_HTTP_POST && mgcf->allow_posting) {
      //Figure out if we want to handle this post request..
      //16 cause we don't really care about what comes after application/json
      if (!r->headers_in.content_type || ngx_strncmp(r->headers_in.content_type->value.data, application_type[0].data, 16) != 0) {
        ngx_http_mg_error(&mg_error, "Incorrect Media Type.", NGX_HTTP_UNSUPPORTED_MEDIA_TYPE, r);
        goto failed;
      }

      rc = ngx_http_read_client_request_body(r, ngx_http_mg_handle_post_request);

      if (rc == NGX_AGAIN) {
        return NGX_AGAIN;
      } else if (rc == NGX_ERROR) {
        ngx_http_mg_error(&mg_error, "There was a problem with the request.", NGX_HTTP_INTERNAL_SERVER_ERROR, r);
        goto failed;
      } else if (rc >= NGX_HTTP_SPECIAL_RESPONSE) {
        return rc;
      } else {
        return NGX_DONE;
      }
    } else {
      ngx_http_mg_error(&mg_error, "Method not allowed.", NGX_HTTP_NOT_ALLOWED, r);
      goto failed;
    }

    return ngx_http_send_response(r, NGX_HTTP_OK, &(application_type[0]), &cv);

    failed:
      return ngx_http_send_response(r, mg_error.error_code, &(application_type[0]), &(mg_error.error_msg));
}

//Build out the complex value from our query and modify our
//request appropriately (handle the application type, etc).
static ngx_int_t
ngx_http_mg_handle_get_request(ngx_http_request_t *r, ngx_http_complex_value_t* cv, error_s* mg_error) {
    //Mongoc related vars
    ngx_http_mg_conf_t  *mgcf = ngx_http_get_module_loc_conf(r, ngx_http_mg_module);

    bson_error_t        error;
    mongoc_cursor_t*    cursor;
    size_t              count;
    size_t              json_length;
    ngx_str_t           query;
    ngx_str_t           is_aggregation;
    ngx_str_t           limit;
    ngx_str_t           offset;
    int                 limit_i;
    int                 offset_i;
    u_char*             json;
    u_char*             response;
    bson_t*             bson_query;

    count = 0;
    json_length = 0;
    json = NULL;
    response = NULL;
    offset_i = 0;
    limit_i = 0;
    bson_query = NULL;
    cursor = NULL;

    ngx_memzero(&query, sizeof(ngx_str_t));
    ngx_memzero(&is_aggregation, sizeof(ngx_str_t));
    ngx_memzero(&limit, sizeof(ngx_str_t));
    ngx_memzero(&offset, sizeof(ngx_str_t));

    ngx_http_arg(r, (u_char *) "q", 1, &query);
    ngx_http_arg(r, (u_char *) "is_aggregation", 14, &is_aggregation);
    ngx_http_arg(r, (u_char *) "offset", 6, &offset);
    ngx_http_arg(r, (u_char *) "limit",  5, &limit);

    if (query.len) {
      //If it's not a star and not empty, let's parse it.
        u_char* pos;
        u_char* dest = (u_char*)ngx_pcalloc(r->pool, query.len);
        pos = dest;
        ngx_unescape_uri(&dest, &(query.data), query.len, 0);
        bson_query = bson_new_from_json(pos, ngx_strlen(pos), &error);

        if (!bson_query) {
          ngx_http_mg_error_from_bson_error(mg_error, &error, r);
          goto failed_before_cursor;
        }
    } else {
      bson_query = bson_new();
    }

    if (offset.len) {
      offset_i = ngx_atoi(offset.data, offset.len); //Convert offset to int
    }

    if (limit.len) {
      limit_i = ngx_atoi(limit.data, limit.len); //Convert limit to int
    }

    if (is_aggregation.len && ngx_strncmp(is_aggregation.data, "true", 4) == 0) {
      cursor = mongoc_collection_aggregate(mgcf->collection, MONGOC_QUERY_NONE, bson_query, NULL, NULL);
    } else {
      cursor = mongoc_collection_find(mgcf->collection, MONGOC_QUERY_NONE, offset_i, limit_i, 0, bson_query, mgcf->unreadable_fields, NULL);
    }

    if (mongoc_cursor_error(cursor, &error) || !cursor) {
      ngx_http_mg_error_from_bson_error(mg_error, &error, r);
      goto failed;
    }

    json = bson_to_json(r, cursor, &json_length, &count, &error);

    if (json == NULL) {
      ngx_http_mg_error_from_bson_error(mg_error, &error, r);
      goto failed;
    }

    //Allocate enough for string + int of 64 bits (20 digits).
    response = ngx_pcalloc(r->pool, json_length + 76 + limit.len + offset.len);

    //If we couldn't allocate, fail.
    if (!response) {
      ngx_http_mg_error(mg_error, "There was a problem setting up the response.", NGX_HTTP_INTERNAL_SERVER_ERROR, r);
      goto failed;
    }

    //55 characters pre-sprintf
    ngx_sprintf(response, "{\"q_results\":[%s],\"count\":%l,\"limit\":%d,\"offset\":%d}", json, (long unsigned)count, limit_i, offset_i);

    ngx_memzero(cv, sizeof(ngx_http_complex_value_t));
    cv->value.data = response;
    cv->value.len = ngx_strlen(response);

    free(json);
    bson_destroy(bson_query);
    mongoc_cursor_destroy(cursor);

    return NGX_OK;

    //We failed after finding, so we can destroy everything.
    failed:
      free(json);
      mongoc_cursor_destroy(cursor);
      bson_destroy(bson_query);

    //We failed trying to parse the json query, so cursor
    //isn't allocated yet and bson_new_from_json destroys the
    //bson_query for us.
    failed_before_cursor:
      free(json);

      return NGX_ERROR;
}

//Handle posting to Mongo.
static void
ngx_http_mg_handle_post_request(ngx_http_request_t* r)  {
    //Since this will just be called as handler, get our conf information.
    ngx_http_mg_conf_t* mgcf = ngx_http_get_module_loc_conf(r, ngx_http_mg_module);

    ngx_http_complex_value_t  cv;
    error_s                   mg_error;
    bson_error_t              error;
    bson_t*                   post_data;
    ngx_chain_t               *cl;
    ngx_buf_t                 *b;
    ngx_uint_t                size;
    ngx_uint_t                total;

    total = 0;
    ngx_uint_t post_buf_size = 4096;
    u_char* post_req = ngx_pcalloc(r->pool, post_buf_size);
    u_char* post_req_pos = post_req;

    ngx_memzero(&cv, sizeof(ngx_http_complex_value_t));

    //Request body is empty.
    if (!r->request_body || !r->request_body->bufs) {
      cv.value.data = (u_char*)"{\"ok\": true, \"reason\":\"Empty post body\"}";
      cv.value.len = ngx_strlen(cv.value.data);
      ngx_http_send_response(r, NGX_HTTP_OK, &(application_type[0]), &cv);
      ngx_http_finalize_request(r, NGX_HTTP_NO_CONTENT);
    }

    for (cl = r->request_body->bufs; cl; cl = cl->next) {
        b = cl->buf;
        size = b->last - b->pos;
        total += size;

        if (total < post_buf_size) {
            post_req = ngx_cpymem(post_req, b->pos, size);
        } else {
            post_buf_size = post_buf_size * 2;
            u_char* temp_post_req = ngx_pcalloc(r->pool, post_buf_size);
            post_req = ngx_cpymem(temp_post_req, post_req_pos, total);
        }
    }

    printf("This should print out the total size: %d", (int)total);
    //Fetch posted data and digest it, then attempt to upsert the data.
    post_data = bson_new_from_json(post_req_pos, total, &error);

    //If not able to create bson from our json
    if (!post_data) {
      ngx_http_mg_error_from_bson_error(&mg_error, &error, r);
      goto failed;
    }

    if (!mongoc_collection_save(mgcf->collection, post_data, NULL, NULL)) {
      ngx_http_mg_error_from_bson_error(&mg_error, &error, r);
      goto failed;
    }

    cv.value.data = (u_char*)"{\"ok\": true, \"reason\":\"Insert successful.\"}";
    cv.value.len = ngx_strlen(cv.value.data);

    bson_free(post_data);

    ngx_http_send_response(r, NGX_HTTP_OK, &(application_type[0]), &cv);
    ngx_http_finalize_request(r, NGX_HTTP_NO_CONTENT);

    return;

    failed:
      bson_free( post_data );

      ngx_http_send_response(r, mg_error.error_code, &(application_type[0]), &(mg_error.error_msg));
      ngx_http_finalize_request(r, NGX_HTTP_NO_CONTENT);
}

static char *
ngx_http_mg(ngx_conf_t *cf, ngx_command_t *cmd, void *conf)
{
  ngx_http_mg_conf_t *mgcf = conf;

  ngx_http_core_loc_conf_t  *clcf;
  ngx_str_t                 *value;
  u_char                    *host_port;
  u_char                    *pos;
  unsigned int              mem_size;
  unsigned int              current_used_mem;
  unsigned int              i;

  clcf = ngx_http_conf_get_module_loc_conf(cf, ngx_http_core_module);
  clcf->handler = ngx_http_mg_handler;

  mem_size = 2048;
  current_used_mem = 0;
  i = 1;
  host_port = calloc(1, mem_size);
  pos = host_port;
  value = cf->args->elts;

  ngx_sprintf(host_port, "%s", DEFAULT_MONGO_ADDR);

  //Loop through our hosts.
  for (; i < cf->args->nelts; i++) {
    if (value[i].len + current_used_mem > mem_size) {
      mem_size += mem_size;
      current_used_mem = 0;

      host_port = realloc(host_port, mem_size);
    }

    host_port = ngx_copy(host_port, value[i].data, value[i].len);

    if (i < cf->args->nelts - 1) {
      host_port = ngx_copy(host_port, ",", 1);
    }

    current_used_mem += value[i].len + 1;
  }

  mgcf->mongo.mongo_server_addr.data = pos;
  mgcf->mongo.mongo_server_addr.len = ngx_strlen(host_port);

  return NGX_CONF_OK;
}

static char*
ngx_http_mg_unreadable_fields(ngx_conf_t *cf, ngx_command_t *cmd, void *conf)
{
  ngx_http_mg_conf_t *mgcf = conf;

  ngx_str_t     *value;
  unsigned int  i;

  i = 1;
  value = cf->args->elts;

  //Start our bson doc, this will be used for our unreadable fields.
  if (cf->args->nelts < 2) {
    mgcf->unreadable_fields = NULL;
  } else {
    mgcf->unreadable_fields = bson_new();
  }

  for (; i < cf->args->nelts; i++) {
    if (!bson_append_int32(mgcf->unreadable_fields, (const char*)value[i].data, value[i].len, 0)){
      return NGX_CONF_ERROR;
    }
  }

  return NGX_CONF_OK;
}

static char*
ngx_http_mg_read_preferences(ngx_conf_t* cf, ngx_command_t* cmd, void* conf) {
  ngx_http_mg_conf_t *mgcf = conf;

  ngx_str_t     *value;
  unsigned int  i;
  char*         tag_key;
  char*         tag_value;

  bson_t b = BSON_INITIALIZER;

  i = 2;
  tag_key = NULL;
  tag_value = NULL;
  value = cf->args->elts;

  if (ngx_strncmp(value[1].data, "primaryPreferred", 16) == 0) {
    mgcf->read_pref = mongoc_read_prefs_new(MONGOC_READ_PRIMARY_PREFERRED);
  } else if (ngx_strncmp(value[1].data, "secondaryPreferred", 18) == 0) {
    mgcf->read_pref = mongoc_read_prefs_new(MONGOC_READ_SECONDARY_PREFERRED);
  } else if (ngx_strncmp(value[1].data, "primary", 7) == 0) {
    mgcf->read_pref = mongoc_read_prefs_new(MONGOC_READ_PRIMARY);
  } else if (ngx_strncmp(value[1].data, "secondary", 9) == 0) {
    mgcf->read_pref = mongoc_read_prefs_new(MONGOC_READ_SECONDARY);
  } else if (ngx_strncmp(value[1].data, "nearest", 7) == 0) {
    mgcf->read_pref = mongoc_read_prefs_new(MONGOC_READ_NEAREST);
  } else {
    ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
        "Must specify one of 4 read preferences: primary, primaryPreferred, secondary, secondaryPreferred, nearest");
    goto failed;
  }

  for (; i < cf->args->nelts; i++) {
    tag_key = strtok((char* __restrict)value[i].data, ":");
    tag_value = strtok(NULL, ":");

    if (tag_value == NULL || tag_key == NULL) {
      ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
          "Tag key/pair was malformatted, should be structured like - key:value");
      goto failed;
    }

    if (!bson_append_utf8(&b, tag_key, ngx_strlen(tag_key), tag_value, ngx_strlen(value))) {
      ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
          "Could not convert tag into bson, please try restructuring");
      goto failed;
    }

    mongoc_read_prefs_add_tag(mgcf->read_pref, &b);
  }

  mongoc_read_prefs_add_tag(mgcf->read_pref, NULL);

  return NGX_CONF_OK;

  failed:
    return NGX_CONF_ERROR;
}

static void *
ngx_http_mg_create_loc_conf(ngx_conf_t *cf)
{

    ngx_http_mg_conf_t  *conf;
    conf = (ngx_http_mg_conf_t *)ngx_pcalloc(cf->pool, sizeof(ngx_http_mg_conf_t));

    conf->allow_posting = NGX_CONF_UNSET;
    conf->allow_aggregation = NGX_CONF_UNSET;

    if (conf == NULL) {
        return NULL;
    }

    /*
     * set by ngx_pcalloc():
     * conf->mongo_server_addr = { 0, NULL }
     * conf->mongo_database = { 0, NULL }
     * conf->mongo_collection = { 0, NULL }
     * conf->mongo_user = { 0, NULL }
     * conf->mongo_passw = { 0, NULL }
     */

    return conf;
}

static char *
ngx_http_mg_merge_loc_conf(ngx_conf_t *cf, void * parent, void * child)
{
    ngx_http_mg_conf_t *prev = (ngx_http_mg_conf_t *) parent;
    ngx_http_mg_conf_t *conf = (ngx_http_mg_conf_t *) child;

    ngx_conf_merge_value(conf->allow_posting, prev->allow_posting, 1);
    ngx_conf_merge_value(conf->allow_aggregation, prev->allow_aggregation, 1);

    ngx_conf_merge_str_value(conf->mongo.mongo_database, prev->mongo.mongo_database, "test");
    ngx_conf_merge_str_value(conf->mongo.mongo_collection, prev->mongo.mongo_collection, "test");

    //Handle connecting to mongo.
    if (conf->mongo.mongo_server_addr.data) {
      conf->uri = ngx_http_create_mongo_conn_uri(conf->mongo);

      if (conf->uri == NULL) {
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                            "Could not create uri = %s", conf->uri);
      }

      conf->client = mongoc_client_new_from_uri(conf->uri);

      if (conf->client == NULL) {
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                            "Could not create client from uri = %s", conf->uri);
        return NGX_CONF_ERROR;
      }

      conf->collection = mongoc_client_get_collection(conf->client, (const char*)conf->mongo.mongo_database.data, (const char*)conf->mongo.mongo_collection.data);

      if (conf->collection == NULL) {
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                            "Could not connect to collection %s at database %s", conf->mongo.mongo_database.data, conf->mongo.mongo_collection.data);
        return NGX_CONF_ERROR;
      }
    }

    //If there's a read preference, set it.
    if (conf->read_pref) {
      mongoc_collection_set_read_prefs(conf->collection, conf->read_pref);
    }

    return NGX_CONF_OK;
}

static void
ngx_http_mg_error(error_s* error, char* reason, ngx_int_t code, ngx_http_request_t* r) {
  size_t reason_len = ngx_strlen(reason);
  u_char* response = ngx_palloc(r->pool, 31 + reason_len);

  ngx_memzero(error, sizeof(error_s));

  ngx_sprintf(response, "{ \"ok\" : false, \"reason\" : \"%s\" }", reason);

  error->error_msg.value.data = (u_char*)response;
  error->error_msg.value.len = ngx_strlen(response);
  error->error_code = code;
}

static void
ngx_http_mg_error_from_bson_error(error_s* error, bson_error_t* bson_error, ngx_http_request_t* r) {
  const char* reason;
  u_char* response = ngx_pcalloc(r->pool, 504);

  ngx_memzero(error, sizeof(error_s));

  switch (bson_error->code) {
    case 11:
    case 13:
      reason = "Not authorized to query this database/collection";
      error->error_code = NGX_HTTP_UNAUTHORIZED;
    break;
    default:
      reason = "Something went wrong. Please try again or alter your configuration";
      error->error_code = NGX_HTTP_INTERNAL_SERVER_ERROR;
    break;
  }

  ngx_sprintf(response, "{\"ok\":false,\"reason\":\"%s\"}", reason);

  error->error_msg.value.data = response;
  error->error_msg.value.len = ngx_strlen(error->error_msg.value.data);
}

static mongoc_uri_t*
ngx_http_create_mongo_conn_uri(mongo_server mongo) {
    uintptr_t escape;
    char* uri_string = NULL;
    char* user_pass = NULL;
    u_char* user = NULL;
    u_char* pass = NULL;

    if (mongo.mongo_user.len) {
      escape = ngx_escape_uri(NULL, mongo.mongo_user.data, mongo.mongo_user.len, NGX_ESCAPE_URI_COMPONENT);
      user = (u_char*) calloc(1, mongo.mongo_user.len + escape);
      escape = ngx_escape_uri(user, mongo.mongo_user.data, mongo.mongo_user.len, NGX_ESCAPE_REFRESH);
    }

    if (mongo.mongo_passw.len) {
      escape = ngx_escape_uri(NULL, mongo.mongo_passw.data, mongo.mongo_passw.len, NGX_ESCAPE_URI_COMPONENT);
      pass = (u_char*) calloc(1, mongo.mongo_passw.len);
      escape = ngx_escape_uri(pass, mongo.mongo_passw.data, mongo.mongo_passw.len, NGX_ESCAPE_URI_COMPONENT);
    }

    //Mongo's not a fan of url's constructed
    //like mongodb://:@host:port, so reconfigure to make pretty..
    //Do we need to do a strlen check?
    if (mongo.mongo_user.len > 0 && mongo.mongo_passw.len > 0) {
      user_pass = bson_strdup_printf("%s:%s@", user, pass);
    } else if (mongo.mongo_user.len > 0) {
      user_pass = bson_strdup_printf("%s@", user);
    }

    uri_string = bson_strdup_printf("mongodb://%s%s/%s",
                                      (user_pass == NULL ? "" : user_pass),
                                      mongo.mongo_server_addr.data,
                                      mongo.mongo_database.data);

    mongoc_uri_t* uri = mongoc_uri_new(uri_string);

    free(user);
    free(pass);
    bson_free(uri_string);
    bson_free(user_pass);

    return uri;
}

u_char *
bson_to_json(ngx_http_request_t* r, mongoc_cursor_t* cursor, size_t *length, size_t* count, bson_error_t* error) {
  bson_string_t* str;
  const bson_t *doc = NULL;
  *count = 0;
  str = bson_string_new("");

  while (!mongoc_cursor_error(cursor, error) && mongoc_cursor_more(cursor) && mongoc_cursor_next(cursor, &doc)) {
    if (*count > 0 && mongoc_cursor_more(cursor)) {
      bson_string_append(str, ",");
    }

    recurse_bson_to_json(r, doc, str, 0);
    *count = *count + 1;
  }

  if (mongoc_cursor_error(cursor, error)) {
    bson_string_free(str, true);
    return NULL;
  }

  *length = str->len;

  return (u_char*)bson_string_free(str, false);
}

static void
recurse_bson_to_json(ngx_http_request_t* r, const bson_t *bson, bson_string_t* str, int is_array) {
  bson_iter_t iter;
  int not_first_key = 0;

  bson_iter_init(&iter, bson); //Todo: Check for NULL

  if (is_array) {
    bson_string_append(str, "[");
  } else {
    bson_string_append(str, "{");
  }

  while(bson_iter_next(&iter)) {

    if (not_first_key) {
      bson_string_append(str, ",");
    }

    not_first_key++;

    if (!is_array) {
      bson_string_append_printf(str, "\"%s\":", bson_iter_key(&iter));
    }

    switch(bson_iter_type(&iter)) {
      case BSON_TYPE_REGEX:
        bson_string_append_printf(str, "%s", bson_iter_regex(&iter, NULL));
        break;
      case BSON_TYPE_UTF8: {
        uint32_t str_len;
        const char* in_str = bson_iter_utf8(&iter, &str_len);
        char* out_str = bson_utf8_escape_for_json(in_str, str_len);
        bson_string_append_printf(str, "\"%s\"", out_str);
        bson_free(out_str);
        break;
      }
      case BSON_TYPE_BOOL:
        bson_string_append_printf(str, "%s", bson_iter_bool(&iter) ? "true" : "false" );
        break;
      case BSON_TYPE_INT64:
        bson_string_append_printf(str, "%" PRIu64, bson_iter_int64(&iter));
        break;
      case BSON_TYPE_INT32:
        bson_string_append_printf(str, "%d", bson_iter_int32(&iter));
        break;
      case BSON_TYPE_DOUBLE:
        bson_string_append_printf(str, "%f", bson_iter_double(&iter));
        break;
      case BSON_TYPE_DOCUMENT: {
        bson_t b;
        uint32_t len;
        const uint8_t *data;

        bson_iter_document(&iter, &len, &data); //Iterate the child obj, put it in data
        bson_init_static(&b, data, len); //From data, create a new child obj (ugh.)

        recurse_bson_to_json(r, &b, str, 0); //Take the child doc and recurse it.
        break;
      }
      case BSON_TYPE_ARRAY: {
        bson_t b;
        uint32_t len;
        const uint8_t *data;

        bson_iter_array(&iter, &len, &data);
        bson_init_static(&b, data, len);

        recurse_bson_to_json(r, &b, str, 1);
        break;
      }
      case BSON_TYPE_OID: {
        char oidhex[25];
        bson_oid_to_string( bson_iter_oid(&iter), oidhex );
        bson_string_append_printf(str, "{ \"$oid\" : \"%s\" }", oidhex);
        break;
      }
      case BSON_TYPE_DATE_TIME:
        bson_string_append_printf(str, "%" PRIu64, bson_iter_date_time(&iter));
      case BSON_TYPE_TIMESTAMP: {
        uint32_t timestamp = (uint32_t)time(NULL);
        bson_iter_timestamp(&iter, &timestamp, NULL);
        bson_string_append_printf(str, "%d", timestamp);
        break;
      }
      case BSON_TYPE_NULL:
        bson_string_append_printf(str, "%s", "null");
        break;
      default:
        break;
    }
  }

  if (is_array) {
    bson_string_append(str, "]");
  } else {
    bson_string_append(str, "}");
  }
}

int replace_str(char *str, char *orig, char *rep)
{
  static char buffer[STR_REPLACE_BUFFER_SIZE];
  char *p;
  if(!(p = strstr(str, orig)))
    return 0;
  strncpy(buffer, str, p-str);
  buffer[p-str] = '\0';
  sprintf(buffer+(p-str), "%s%s", rep, p+strlen(orig));
  memcpy(str,buffer,strlen(buffer)+1);
  return 1;
}

